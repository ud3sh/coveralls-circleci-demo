package com.streetcontxt.coverage_demo;

import org.immutables.value.Value;


@Value.Immutable
public interface Tree<T extends Comparable> {
    Node<T> getRoot();
}

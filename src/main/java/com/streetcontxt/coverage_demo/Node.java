package com.streetcontxt.coverage_demo;

import java.util.Optional;

import org.immutables.value.Value;

@Value.Immutable
public interface Node<T extends Comparable> {
    T getValue();
    Optional<Node<T>> getLeft();
    Optional<Node<T>> getRight();
}

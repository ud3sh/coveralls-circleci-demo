package com.streetcontxt.coverage_demo;

import java.util.ArrayList;

import java.util.List;

public final class TreeOperations {

    private TreeOperations() {}

    public static <T extends Comparable> List<T> traverseInOrder(final Node<T> node){
        List<T> result = new ArrayList<T>();
        result.add(node.getValue());
        if (node.getLeft().isPresent()) {
            result.addAll(traverseInOrder(node.getLeft().get()));
        }

        if (node.getRight().isPresent()) {
            result.addAll(traverseInOrder(node.getRight().get()));
        }

        return result;
    }

    public static <T extends Comparable> boolean isLeaf(final Node<T> node) {
        if (node.getRight().isPresent() || node.getLeft().isPresent()) {
            return false;
        } else {
            return true;
        }
    }

}
